package com.dep.bookApp.controllers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.List;
import com.dep.bookApp.models.Book;
import com.dep.bookApp.services.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest {
	@InjectMocks
	private BookController bookController;

	@Mock
	private BookService bookService;

	@Test
	public void findBooks_ReturnsNoContent() {
		when(bookService.findBooks(anyString(), anyString())).thenReturn( Collections.emptyList());

		ResponseEntity<List<Book>> response1 = bookController.findBooks("Author1", "Comedy");
		ResponseEntity<List<Book>> response2 = bookController.findBooks("Author2", "Drama");

		assertEquals(204, response1.getStatusCode().value());
		assertEquals(204, response2.getStatusCode().value());
	}

	@Test
	public void findBooks_ReturnsListOfBooks() {
		Book book = new Book();
		book.setTitle("Title");
		book.setAuthor("Author");
		book.setGenre("Comedy");

		when(bookService.findBooks( anyString(), anyString())).thenReturn( List.of( book ) );

		ResponseEntity<List<Book>> response1 = bookController.findBooks( "Author", "Comedy");

		assertEquals(200, response1.getStatusCode().value());
		assertEquals(1, response1.getBody().size());
		assertEquals("Title", response1.getBody().get(0).getTitle());
		assertEquals("Author", response1.getBody().get(0).getAuthor());
		assertEquals("Comedy", response1.getBody().get(0).getGenre());

		ResponseEntity<List<Book>> response2 = bookController.findBooks( "Test", "Drama");

		assertEquals(200, response2.getStatusCode().value());
		assertEquals(1, response2.getBody().size());
		assertEquals("Title", response2.getBody().get(0).getTitle());
		assertEquals("Author", response2.getBody().get(0).getAuthor());
		assertEquals("Comedy", response2.getBody().get(0).getGenre());
	}
}
