package com.dep.bookApp.models;
import lombok.Data;

@Data
public class Book {
	private String title;
	private String author;
	private String genre;
	private String description;

}
