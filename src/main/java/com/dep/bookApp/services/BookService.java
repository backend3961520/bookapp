package com.dep.bookApp.services;
import java.util.List;
import java.util.stream.Collectors;
import com.dep.bookApp.models.Book;
import org.springframework.stereotype.Service;

@Service
public class BookService {

	public List<Book> findBooks(String author, String genre) {
		Book book1 = createBook("Book1", "Author1", "Comedy", "Description 1");
		Book book2 = createBook("Book2", "Author2", "Drama", "Description 2");
		Book book3 = createBook("Book3", "Author3", "Love", "Description 3");

		return filterBooks( List.of(book1, book2, book3), author, genre);
	}

	private Book createBook(String title, String author, String genre, String description) {
		Book book = new Book();
		book.setTitle(title);
		book.setAuthor(author);
		book.setGenre(genre);
		book.setDescription(description);

		return book;
	}

	private List<Book> filterBooks(List<Book> books, String author, String genre) {
		return books.stream()
				.filter(book -> book.getAuthor().equals(author) && book.getGenre().equals(genre))
				.collect( Collectors.toList());
	}
}
