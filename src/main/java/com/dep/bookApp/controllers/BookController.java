package com.dep.bookApp.controllers;
import java.util.List;
import com.dep.bookApp.models.Book;
import com.dep.bookApp.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class BookController {
	private final BookService bookService;

	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@GetMapping("/")
	public ResponseEntity<List<Book>> findBooks(@RequestParam String author, @RequestParam String genre) {
		List<Book> books = bookService.findBooks(author, genre);

		if (books.isEmpty()) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(books);
		}
	}
}
